package info.sabtuminggu.rizqi_1202164364_si4004_pab_modul2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CheckoutActivity extends AppCompatActivity {
    TextView tvTanggalBerangkat, tvTanggalPulang, tvJumlahTiket, tvHargaTotal, tvTujuan;
    Button btnKonfirmasi;
    String tujuan, tanggalBerangkat, tanggalPulang, jumlahTiket, hargaTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onCreate");
        tvTujuan = findViewById(R.id.tv_tujuan_summary);
        tvTanggalBerangkat = findViewById(R.id.tv_tanggal_berangkat_summary);
        tvTanggalPulang = findViewById(R.id.tv_tanggal_pulang_summary);
        tvJumlahTiket = findViewById(R.id.tv_jumlah_tiket_summary);
        tvHargaTotal = findViewById(R.id.tv_harga_total_summary);
        btnKonfirmasi = findViewById(R.id.btn_konfirmasi);

//        Ambil Titipannya

        Intent extras = getIntent();
        if (extras != null) {
            tujuan = extras.getStringExtra("tujuan").replaceAll("[^a-zA-Z0-9]", "").replaceAll("Rp","").replaceAll("\\d","");
            tanggalBerangkat = extras.getStringExtra("tanggalBerangkat");
            tanggalPulang = extras.getStringExtra("tanggalPulang");
            jumlahTiket = extras.getStringExtra("jumlahTiket");
            hargaTotal = extras.getStringExtra("hargaTotal");
            tvTujuan.setText(tujuan);
            tvTanggalBerangkat.setText(tanggalBerangkat);
            tvTanggalPulang.setText(tanggalPulang);
            tvJumlahTiket.setText(jumlahTiket);
            tvHargaTotal.setText("Rp " + hargaTotal);
        }

        btnKonfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                setResult(1, i);
                finish();
            }
        });
    }


//        Start Log Activity LifeCycle
    @Override
    protected void onStart() {
        super.onStart();
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onPause");
    }
//    End Log Activity LifeCycle
}
