package info.sabtuminggu.rizqi_1202164364_si4004_pab_modul2;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

public class SplashScreen extends AppCompatActivity{
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//         remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
//        remove action bar
        actionBar = getSupportActionBar();
        actionBar.hide();
        Log.d("PAB/"+this.getClass().getSimpleName(), "ini onCreate");
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
            }
        }, 3000L); //3000 L = 3 detik

    }
//        Start Log Activity LifeCycle
    @Override
    protected void onStart() {
        super.onStart();
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onPause");
    }
//        End Log Activity LifeCycle
}
