package info.sabtuminggu.rizqi_1202164364_si4004_pab_modul2;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Switch swPulangPergi;
    TextView tvPilihTanggalPulang, tvPilihWaktuPulang, tvTopUp, tvSaldo, tvPilihTanggalBerangkat, tvPilihWaktuBerangkat, tvTerimaKasih;
    DatePickerDialog datePickerDialog;
    Button btnBeliTiket;
    EditText edtJumlahTiket;
    String tanggalBerangkat, tanggalPulang, strJumlahTiket, strHargaTotal, tujuan, waktuBerangkat, waktuPulang;
    int saldoSementara, hargaTiket, jumlahTiket, totalTransaksi;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onCreate");

        tvPilihTanggalPulang = findViewById(R.id.tv_pilih_tanggal_pulang);
        tvPilihWaktuPulang = findViewById(R.id.tv_pilih_waktu_pulang);
        tvPilihTanggalBerangkat = findViewById(R.id.tv_pilih_tanggal_berangkat);
        tvPilihWaktuBerangkat = findViewById(R.id.tv_pilih_waktu_berangkat);
        tvTopUp = findViewById(R.id.tv_topup);
        tvSaldo = findViewById(R.id.tv_saldo);
        tvTerimaKasih = findViewById(R.id.tv_terima_kasih);
        btnBeliTiket = findViewById(R.id.btn_belitiket);
        edtJumlahTiket = findViewById(R.id.edt_jumlahtiket);
        saldoSementara = Integer.parseInt(tvSaldo.getText().toString());
        swPulangPergi = (Switch) findViewById(R.id.sw_pulangpergi);

//        Start Spinner
        spinner = (Spinner) findViewById(R.id.spinner);
        // Spinner click listener
        spinner.setOnItemSelectedListener(this);
        // Spinner Drop down elements
        List<String> listTujuan = new ArrayList<String>();
        listTujuan.add("Jakarta (Rp 85.000)");
        listTujuan.add("Cirebon (Rp 150.000)");
        listTujuan.add("Bekasi (Rp 70.000)");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listTujuan);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
//        End Spinner

//        Start Switch
        swPulangPergi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String statusSwitch = String.valueOf(isChecked);
                if (isChecked) {
                    tvPilihTanggalPulang.setVisibility(View.VISIBLE);
                    tvPilihWaktuPulang.setVisibility(View.VISIBLE);
                } else {
                    tvPilihTanggalPulang.setVisibility(View.INVISIBLE);
                    tvPilihWaktuPulang.setVisibility(View.INVISIBLE);
                }
            }
        });
//        End Switch

//        Fungsi Top Up
        tvTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialogTopTup();
            }
        });

//       Fungsi Pilih Tanggal Berangkat
        tvPilihTanggalBerangkat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pilihTanggal(tvPilihTanggalBerangkat);
            }
        });

//      Fungsi Pilih Waktu Berangkat
        tvPilihWaktuBerangkat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pilihWaktu(tvPilihWaktuBerangkat);
            }
        });

//      Fungsi Pilih Tanggal Pulang
        tvPilihTanggalPulang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pilihTanggal(tvPilihTanggalPulang);
            }
        });

//      Fungsi Pilih Waktu Pulang
        tvPilihWaktuPulang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pilihWaktu(tvPilihWaktuPulang);
            }
        });

//      Fungsi Button Beli Tiket
        btnBeliTiket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    tujuan = spinner.getSelectedItem().toString();
                    tanggalBerangkat = tvPilihTanggalBerangkat.getText().toString();
                    tanggalPulang = tvPilihTanggalPulang.getText().toString();
                    waktuBerangkat = tvPilihWaktuBerangkat.getText().toString() + " WIB" ;
                    waktuPulang = tvPilihWaktuPulang.getText().toString() + " WIB" ;
                    strJumlahTiket = edtJumlahTiket.getText().toString();
                    hargaTiket = Integer.parseInt(tujuan.replaceAll("[\\D]", ""));
//                Jika pulang pergi maka harganya 2 kali ipat
                    if (swPulangPergi.isChecked()) {
                        hargaTiket = hargaTiket * 2;
                    }
//                int saldo = Integer.parseInt(tvSaldo.getText().toString().replaceAll("[\\D]", ""));
                    jumlahTiket = Integer.parseInt(edtJumlahTiket.getText().toString());
                    totalTransaksi = hargaTiket * jumlahTiket;
                    strHargaTotal = Integer.toString(totalTransaksi);

//                    Cek Saldo Apakah Melebihi Total Transaksi atau tidak
                    if (saldoSementara >= totalTransaksi && jumlahTiket > 0) {
                        Toast.makeText(MainActivity.this,
                                "Bisa Transaksi",
                                Toast.LENGTH_SHORT).show();
//                    Kalo Belum Dipilih
                        if (tanggalBerangkat.equals("Pilih Tanggal")) {
                            tanggalBerangkat = "null";
                        }
                        if (tanggalPulang.equals("Pilih Tanggal")) {
                            tanggalPulang = "null";
                        }
                        if (waktuBerangkat.equals("Pilih Waktu")) {
                            waktuBerangkat = "null";
                        }
                        if (waktuPulang.equals("Pilih Waktu")) {
                            waktuPulang = "null";
                        }
//                    Kalo ngga pulang pergi
                        if (swPulangPergi.isChecked() == false) {
                            tanggalPulang = "null";
                        }

                        Intent i = new Intent(MainActivity.this, CheckoutActivity.class);
                        i.putExtra("tujuan", tujuan);
                        i.putExtra("tanggalBerangkat", tanggalBerangkat + "-" + waktuBerangkat );
                        i.putExtra("tanggalPulang", tanggalPulang + "-" + waktuPulang );
                        i.putExtra("jumlahTiket", strJumlahTiket);
                        i.putExtra("hargaTotal", strHargaTotal);
                        startActivityForResult(i, 1);
                    } else {
                        Toast.makeText(MainActivity.this,
                                "Tidak Bisa Transaksi Saldo Kurang, Silahkan Top Up Dahulu atau tiket kurang dari satu",
                                Toast.LENGTH_SHORT).show();
                        tvTerimaKasih.setVisibility(View.INVISIBLE);
                    }
                } catch (Exception e) {
                    edtJumlahTiket.setError("Ini Diisi Dulu");
                    tvTerimaKasih.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

//        Start Log Activity LifeCycle
    @Override
    protected void onResume() {
        super.onResume();
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onResume");
//        tvTerimaKasih.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onDestroy");
    }
//    End Log Activity LifeCycle

    //  Fungsi Top Up
    public void openDialogTopTup() {

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        // Set Custom Title
        TextView title = new TextView(this);
        // Title Properties
        title.setText("Masukkan Jumlah Saldo");
        title.setPadding(10, 10, 10, 10);   // Set Position
        title.setGravity(Gravity.CENTER);
        title.setTextColor(Color.BLACK);
        title.setTextSize(20);
        alertDialog.setCustomTitle(title);

        // Set EditText
        final EditText topUpSaldo = new EditText(this);
        // Message Properties
        topUpSaldo.setGravity(Gravity.CENTER_HORIZONTAL);
        topUpSaldo.setTextColor(Color.BLACK);
        topUpSaldo.setInputType(InputType.TYPE_CLASS_NUMBER);
        alertDialog.setView(topUpSaldo);

        // Set Button
        // you can more buttons
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Tambah Saldo", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                try {
                    saldoSementara = saldoSementara + Integer.parseInt(topUpSaldo.getText().toString());
                    tvSaldo.setText(Integer.toString(saldoSementara));
                    Toast.makeText(MainActivity.this,
                            "Top Up Berhasil",
                            Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    topUpSaldo.setError("Jumlah Saldo Top Up Harus Diisi");
                }
            }
        });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Perform Action on Button
            }
        });

        new Dialog(getApplicationContext());
        alertDialog.show();
    }

    public void pilihTanggal(final TextView tv) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        datePickerDialog = new DatePickerDialog(MainActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        tv.setText(
                                dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public void pilihWaktu(final TextView tv) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                tv.setText(selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Pilih Waktu");
        mTimePicker.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == 1)
        {
            saldoSementara = saldoSementara - totalTransaksi;
            tvSaldo.setText(Integer.toString(saldoSementara));
//                    Munculkan TextView TerimaKasih Telah Bertransaksi
              tvTerimaKasih.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
